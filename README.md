# Webhook-ir-remote
Replace the IR remote of LED strips with an ESP8266 and an IR LED. The ESP8266 acts as a webserver. When a get request to a specific path is received, an IR signal is sent.
The request can be sent via GET request, or via the webpage served by the ESP.

![Webinterface](assets/webinterface_ir_remote.png)
![Darstellung des Aufbaus](assets/552px-Aufbau_IR_Remote.png)